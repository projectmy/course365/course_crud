       /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */

       var gCoursesDB = {
           description: "This DB includes all courses in system",
           courses: [{
               id: 1,
               courseCode: "FE_WEB_ANGULAR_101",
               courseName: "How to easily create a website with Angular",
               price: 750,
               discountPrice: 600,
               duration: "3h 56m",
               level: "Beginner",
               coverImage: "images/courses/course-angular.jpg",
               teacherName: "Morris Mccoy",
               teacherPhoto: "images/teacher/morris_mccoy.jpg",
               isPopular: false,
               isTrending: true
           }, {
               id: 2,
               courseCode: "BE_WEB_PYTHON_301",
               courseName: "The Python Course: build web application",
               price: 1050,
               discountPrice: 900,
               duration: "4h 30m",
               level: "Advanced",
               coverImage: "images/courses/course-python.jpg",
               teacherName: "Claire Robertson",
               teacherPhoto: "images/teacher/claire_robertson.jpg",
               isPopular: false,
               isTrending: true
           }, {
               id: 5,
               courseCode: "FE_WEB_GRAPHQL_104",
               courseName: "GraphQL: introduction to graphQL for beginners",
               price: 850,
               discountPrice: 650,
               duration: "2h 15m",
               level: "Intermediate",
               coverImage: "images/courses/course-graphql.jpg",
               teacherName: "Ted Hawkins",
               teacherPhoto: "images/teacher/ted_hawkins.jpg",
               isPopular: true,
               isTrending: false
           }, {
               id: 6,
               courseCode: "FE_WEB_JS_210",
               courseName: "Getting Started with JavaScript",
               price: 550,
               discountPrice: 300,
               duration: "3h 34m",
               level: "Beginner",
               coverImage: "images/courses/course-javascript.jpg",
               teacherName: "Ted Hawkins",
               teacherPhoto: "images/teacher/ted_hawkins.jpg",
               isPopular: true,
               isTrending: true
           }, {
               id: 8,
               courseCode: "FE_WEB_CSS_111",
               courseName: "CSS: ultimate CSS course from beginner to advanced",
               price: 750,
               discountPrice: 600,
               duration: "3h 56m",
               level: "Beginner",
               coverImage: "images/courses/course-css.jpg",
               teacherName: "Juanita Bell",
               teacherPhoto: "images/teacher/juanita_bell.jpg",
               isPopular: true,
               isTrending: true
           }, {
               id: 14,
               courseCode: "FE_WEB_WORDPRESS_111",
               courseName: "Complete Wordpress themes & plugins",
               price: 1050,
               discountPrice: 900,
               duration: "4h 30m",
               level: "Advanced",
               coverImage: "images/courses/course-wordpress.jpg",
               teacherName: "Clevaio Simon",
               teacherPhoto: "images/teacher/clevaio_simon.jpg",
               isPopular: true,
               isTrending: false
           }]
       }

       const gCOURSE_COL = ["id", "courseCode", "courseName", "price", "discountPrice", "duration", "level", "coverImage", "teacherName", "teacherPhoto", "isPopular", "isTrending", "action"];

       const gID_COL = 0;
       const gCOURSE_CODE_COL = 1;
       const gCOURSE_NAME_COL = 2;
       const gPRICE_COL = 3;
       const gDISCOUNT_PRICE_COL = 4;
       const gDURATION_COL = 5;
       const gLEVEL_COL = 6;
       const gCOVER_IMAGE_COL = 7;
       const gTEACHER_NAME_COL = 8;
       const gTEACHER_PHOTO_COL = 9;
       const gIS_POPULAR_COL = 10;
       const gIS_TRENDING_COL = 11;
       const gACTION_COL = 12;

       //Biến lưu ID Courses
       var gCoursesId = "";


       /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
       $(document).ready(function() {

           onPageLoading();

           // 2 - C: Gán sự kiện Create - Thêm mới courses
           $("#btn-add-courses").on("click", function() {
               onBtnAddNewCoursesClick();
           });
           // gán sự kiện cho nút Create Course (trên modal)
           $("#btn-create-courses").on("click", function() {
               onBtnCreateCoursesClick();
           });



           // 3 - U: gán sự kiện Update - Sửa 1 Course
           $("#courses-table").on("click", ".btn-edit-course", function() {
               onBtnEditCourseClick(this);
           });

           // gán sự kiện cho nút Update Course (trên modal)
           $("#btn-upDate-course").on("click", function() {
               onBtnUpdateCourseClick();
           });


           // 4 - D: gán sự kiện Delete - Xóa 1 Course
           $("#courses-table").on("click", ".btn-delete-course", function() {
               onBtnDeleteCourseClick(this);
           });

           $("#btn-confirm-delete-course").on("click", function() {
               onBtnConfirmDeleteCourseClick();
           });
       });



       /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */

       function onPageLoading() {
           "use strict";

           $('#courses-table').DataTable({

               //    data: gCoursesDB.courses,
               columns: [{
                   data: gCOURSE_COL[gID_COL]
               }, {
                   data: gCOURSE_COL[gCOURSE_CODE_COL]
               }, {
                   data: gCOURSE_COL[gCOURSE_NAME_COL]
               }, {
                   data: gCOURSE_COL[gPRICE_COL]
               }, {
                   data: gCOURSE_COL[gDISCOUNT_PRICE_COL]
               }, {
                   data: gCOURSE_COL[gDURATION_COL]
               }, {
                   data: gCOURSE_COL[gLEVEL_COL]
               }, {
                   data: gCOURSE_COL[gCOVER_IMAGE_COL]
               }, {
                   data: gCOURSE_COL[gTEACHER_NAME_COL]
               }, {
                   data: gCOURSE_COL[gTEACHER_PHOTO_COL]
               }, {
                   data: gCOURSE_COL[gIS_POPULAR_COL]
               }, {
                   data: gCOURSE_COL[gIS_TRENDING_COL]
               }, {
                   data: gCOURSE_COL[gACTION_COL]
               }],

               columnDefs: [{
                   targets: gACTION_COL,
                   defaultContent: `
                    <i class="fas fa-edit text-success pointer ml-3 btn btn-edit-course" data-toggle="tooltip" title="Sửa đơn hàng"></i>
                    <i class="fas fa-trash-alt text-danger pointer ml-3 btn btn-delete-course" data-toggle="tooltip" title="Xóa đơn hàng"></i>
    `
               }]
           });

           onLoadCoursesToTable(gCoursesDB.courses);

       }
       /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

       //Hàm Load Table dùng chung cho các hàm
       function onLoadCoursesToTable(paramData) {

           var vTable = $('#courses-table').DataTable();
           vTable.clear();
           vTable.rows.add(paramData);
           vTable.draw();
       }



       // CÁC HÀM CHO ADD NEW VÀ CREAT

       // Hàm sự kiện nút add new
       function onBtnAddNewCoursesClick() {

           $('#insert-creat-courses-modal').modal('show');
       }

       // Hàm sự kiện nút creat - thêm dữ liệu mới
       function onBtnCreateCoursesClick() {

           var vCoursesObj = {
               id: 0,
               courseCode: "",
               courseName: "",
               price: 1,
               discountPrice: 1,
               duration: "",
               level: "",
               coverImage: "",
               teacherName: "",
               teacherPhoto: "",
               isPopular: "",
               isTrending: ""
           }

           // B1: Thu thập dữ liệu
           getAddNewCoursesDataCreate(vCoursesObj)

           // B2: Validate insert
           var vIsCourseValidate = validateAddNewCourseData(vCoursesObj);
           if (vIsCourseValidate) {

               // B3: insert course
               insertCourses(vCoursesObj);

               // B4: xử lý front-end
               alert("Thêm course thành công!");
               onLoadCoursesToTable(gCoursesDB.courses);
               resertCreateCoursesForm();
               $('#insert-creat-courses-modal').modal('hide');
           }

       }

       // hàm thu thập dữ liệu để insert courses
       function getAddNewCoursesDataCreate(paramCourseObj) {
           "use strict";

           paramCourseObj.id = getNextId();
           paramCourseObj.courseCode = $("#inp-creat-courseCode").val().trim();
           paramCourseObj.courseName = $("#select-creat-courses").val();
           paramCourseObj.price = parseInt($("#inp-creat-price").val());
           paramCourseObj.discountPrice = parseInt($("#inp-creat-discountPrice").val());
           paramCourseObj.duration = $("#inp-creat-duration").val().trim();
           paramCourseObj.level = $("#inp-creat-level").val().trim();
           paramCourseObj.coverImage = $("#inp-creat-coverImage").val().trim();
           paramCourseObj.teacherName = $("#inp-creat-teacherName").val().trim();
           paramCourseObj.teacherPhoto = $("#inp-creat-teacherPhoto").val().trim();
           paramCourseObj.isPopular = $("#select-creat-popular").val();
           paramCourseObj.isTrending = $("#select-creat-trending").val();

       }
       // hàm sinh ra đc id tự tăng tiếp theo, dùng khi Thêm mới phần tử
       function getNextId() {
           var vNextId = 0;
           // Nếu mảng chưa có đối tượng nào thì Id = 1
           if (gCoursesDB.courses.length == 0) {
               vNextId = 1;
           }
           // Id tiếp theo bằng Id của phần tử cuối cùng + thêm 1
           else {
               vNextId = gCoursesDB.courses[gCoursesDB.courses.length - 1].id + 1;
           }
           return vNextId;
       }


       function validateAddNewCourseData(paramCourseObj) {

           if (paramCourseObj.courseCode == "") {
               console.log("Dữ liệu ô Course Code không được để trống");
               alert("Dữ liệu ô Course Code không được để trống");
               return false;
           }

           if (paramCourseObj.courseName == "NOT_SELECT_COURSES") {
               console.log("Phải chọn khóa học");
               alert("Phải chọn khóa học");
               return false;
           }

           if (paramCourseObj.price == "" || paramCourseObj.price < 1) {
               console.log("Dữ liệu ô Price không đúng");
               alert("Dữ liệu ô Price không đúng");
               return false;
           }

           if (isNaN(paramCourseObj.price)) {
               console.log("Price phải nhập số");
               alert("Price phải nhập số");
               return false;
           }



           if (paramCourseObj.discountPrice == "" || paramCourseObj.discountPrice < 0) {
               console.log("Dữ liệu ô Discount Price không đúng");
               alert("Dữ liệu ô Discount Price không đúng");
               return false;
           }

           if (isNaN(paramCourseObj.discountPrice)) {
               console.log("Discount Price phải nhập số");
               alert("Discount Price phải nhập số");
               return false;
           }



           if (paramCourseObj.duration == "") {
               console.log("Dữ liệu ô Duration không được để trống");
               alert("Dữ liệu ô Duration không được để trống");
               return false;
           }

           if (paramCourseObj.level == "") {
               console.log("Dữ liệu ô level không được để trống");
               alert("Dữ liệu ô level không được để trống");
               return false;
           }

           if (paramCourseObj.coverImage == "") {
               console.log("Dữ liệu ô coverImage không được để trống");
               alert("Dữ liệu ô coverImage không được để trống");
               return false;
           }

           if (paramCourseObj.teacherName == "") {
               console.log("Dữ liệu ô Teacher Name không được để trống");
               alert("Dữ liệu ô Teacher Name không được để trống");
               return false;
           }

           if (paramCourseObj.teacherPhoto == "") {
               console.log("Dữ liệu ô Teacher Photo không được để trống");
               alert("Dữ liệu ô Teacher Photo không được để trống");
               return false;
           }

           if (paramCourseObj.isPopular == "NOT_SELECT_POPULAR") {
               console.log("Hãy chọn trạng thái ô Popular");
               alert("Hãy chọn trạng thái ô Popular");
               return false;
           }

           if (paramCourseObj.isTrending == "NOT_SELECT_TRENDING") {
               console.log("Hãy chọn trạng thái ô Trending");
               alert("Hãy chọn trạng thái ô Trending");
               return false;
           }

           return true;
       }

       // hàm thực hiện insert Course vào mảng
       function insertCourses(paramCoursesObj) {
           gCoursesDB.courses.push(paramCoursesObj);
       }

       // hàm xóa trắng form modal create Course
       function resertCreateCoursesForm() {

           $("#inp-creat-courseCode").val("");
           $("#select-creat-courses").val("");
           $("#inp-creat-price").val("");
           $("#inp-creat-discountPrice").val("");
           $("#inp-creat-duration").val("");
           $("#inp-creat-level").val("");
           $("#inp-creat-coverImage").val("");
           $("#inp-creat-teacherName").val("");
           $("#inp-creat-teacherPhoto").val("");
           $("#select-creat-popular").val("");
           $("#select-creat-trending").val("");
       }




       // CÁC HÀM CHO EDIT VÀ UPDATE

       // hàm xử lý sự kiện edit courses modal click
       function onBtnEditCourseClick(paramEdit) {

           // lưu thông tin Id đang được edit vào biến
           gCoursesId = getCoursesIdFormButton(paramEdit);

           // load dữ liệu vào các trường dữ liệu trong modal
           showCoursesDataToModal(gCoursesId);

           // hiển thị modal lên
           $("#insert-upDate-courses-modal").modal("show");
       }

       // hàm show courses obj lên modal
       function showCoursesDataToModal(paramCoursesId) {
           var vCourseIndex = getIndexFormCoursesId(paramCoursesId);

           $("#result-upDate-id").html(gCoursesDB.courses[vCourseIndex].id);
           $("#result-upDate-courseCode").html(gCoursesDB.courses[vCourseIndex].courseCode);
           $("#inp-upDate-courses").val(gCoursesDB.courses[vCourseIndex].courseName);
           $("#inp-upDate-price").val(gCoursesDB.courses[vCourseIndex].price);
           $("#inp-upDate-discountPrice").val(gCoursesDB.courses[vCourseIndex].discountPrice);
           $("#inp-upDate-duration").val(gCoursesDB.courses[vCourseIndex].duration);
           $("#inp-upDate-level").val(gCoursesDB.courses[vCourseIndex].level);
           $("#inp-upDate-coverImage").val(gCoursesDB.courses[vCourseIndex].coverImage);
           $("#inp-upDate-teacherName").val(gCoursesDB.courses[vCourseIndex].teacherName);
           $("#inp-upDate-teacherPhoto").val(gCoursesDB.courses[vCourseIndex].teacherPhoto);
           $("#inp-upDate-popular").val(gCoursesDB.courses[vCourseIndex].isPopular);
           $("#inp-upDate-trending").val(gCoursesDB.courses[vCourseIndex].isTrending);
       }


       // hàm xử lý sự kiện update courses modal click
       function onBtnUpdateCourseClick() {

           // khai báo đối tượng chứa courses data
           var vCoursesObj = {
               id: 0,
               courseCode: "",
               courseName: "",
               price: 1,
               discountPrice: 1,
               duration: "",
               level: "",
               coverImage: "",
               teacherName: "",
               teacherPhoto: "",
               isPopular: "",
               isTrending: ""
           }

           // B1: Thu thập dữ liệu
           getUpdateCoursesData(vCoursesObj)

           // B2: Validate insert
           var vIsCourseValidate = validateUpdateCourseData(vCoursesObj);
           if (vIsCourseValidate) {

               // B3: insert course
               updateCourses(vCoursesObj);

               // B4: xử lý front-end
               alert("Sửa course thành công!");
               onLoadCoursesToTable(gCoursesDB.courses);
               resertUpdateCoursesForm();
               $('#insert-upDate-courses-modal').modal('hide');
           }
       }

       function getUpdateCoursesData(paramCourseObj) {

           paramCourseObj.id = $("#result-upDate-id").html();
           paramCourseObj.courseCode = $("#result-upDate-courseCode").html();
           paramCourseObj.courseName = $("#inp-upDate-courses").val();
           paramCourseObj.price = parseInt($("#inp-upDate-price").val());
           paramCourseObj.discountPrice = parseInt($("#inp-upDate-discountPrice").val());
           paramCourseObj.duration = $("#inp-upDate-duration").val().trim();
           paramCourseObj.level = $("#inp-upDate-level").val().trim();
           paramCourseObj.coverImage = $("#inp-upDate-coverImage").val().trim();
           paramCourseObj.teacherName = $("#inp-upDate-teacherName").val().trim();
           paramCourseObj.teacherPhoto = $("#inp-upDate-teacherPhoto").val().trim();
           paramCourseObj.isPopular = $("#inp-upDate-popular").val();
           paramCourseObj.isTrending = $("#inp-upDate-trending").val();
       }


       function validateUpdateCourseData(paramCourseObj) {

           if (paramCourseObj.courseName === "") {
               console.log("Dữ liệu ô Course Name không được để trống");
               alert("Dữ liệu ô Course Name không được để trống");
           }


           if (paramCourseObj.price === "" || paramCourseObj.price < 1) {
               console.log("Dữ liệu ô Price không đúng");
               alert("Dữ liệu ô Price không đúng");
               return false;
           }

           if (isNaN(paramCourseObj.price)) {
               console.log("Price phải nhập số");
               alert("Price phải nhập số");
               return false;
           }



           if (paramCourseObj.discountPrice === "" || paramCourseObj.discountPrice < 0) {
               console.log("Dữ liệu ô Discount Price không đúng");
               alert("Dữ liệu ô Discount Price không đúng");
               return false;
           }

           if (isNaN(paramCourseObj.discountPrice)) {
               console.log("Discount Price phải nhập số");
               alert("Discount Price phải nhập số");
               return false;
           }



           if (paramCourseObj.duration === "") {
               console.log("Dữ liệu ô Duration không được để trống");
               alert("Dữ liệu ô Duration không được để trống");
               return false;
           }

           if (paramCourseObj.level === "") {
               console.log("Dữ liệu ô level không được để trống");
               alert("Dữ liệu ô level không được để trống");
               return false;
           }

           if (paramCourseObj.coverImage === "") {
               console.log("Dữ liệu ô coverImage không được để trống");
               alert("Dữ liệu ô coverImage không được để trống");
               return false;
           }

           if (paramCourseObj.teacherName === "") {
               console.log("Dữ liệu ô Teacher Name không được để trống");
               alert("Dữ liệu ô Teacher Name không được để trống");
               return false;
           }

           if (paramCourseObj.teacherPhoto === "") {
               console.log("Dữ liệu ô Teacher Photo không được để trống");
               alert("Dữ liệu ô Teacher Photo không được để trống");
               return false;
           }

           if (paramCourseObj.isPopular === "") {
               console.log("Dữ liệu ô Popular không được để trống");
               alert("Dữ liệu ô Teacher Popular không được để trống");
               return false;
           }

           if (paramCourseObj.isTrending === "") {
               console.log("Dữ liệu ô Trending không được để trống");
               alert("Dữ liệu ô Trending không được để trống");
               return false;
           }

           return true;
       }

       // hàm thực hiện update course vào mảng
       function updateCourses(paramCourseObj) {

           var vCourseIndex = getIndexFormCoursesId(gCoursesId);
           gCoursesDB.courses.splice(vCourseIndex, 1, paramCourseObj);
       }

       // hàm xóa trắng form modal update courses
       function resertUpdateCoursesForm() {

           $("#result-upDate-id").html("");
           $("#result-upDate-courseCode").html("");
           $("#inp-upDate-courses").val("");
           $("#inp-upDate-price").val("");
           $("#inp-upDate-discountPrice").val("");
           $("#inp-upDate-duration").val("");
           $("#inp-upDate-level").val("");
           $("#inp-upDate-coverImage").val("");
           $("#inp-upDate-teacherName").val("");
           $("#inp-upDate-teacherPhoto").val("");
           $("#inp-upDate-popular").val("");
           $("#inp-upDate-trending").val("");
       }


       // CÁC HÀM CHO DELETE


       // Hàm xử lý sự kiện khi icon delete voucher trên bảng đc click
       function onBtnDeleteCourseClick(paramBtnDelete) {
           // lưu thông tin CourseId đang được delete vào biến toàn cục
           gCoursesId = getCoursesIdFormButton(paramBtnDelete);
           // hiển thị modal confirm xóa lên
           $("#delete-confirm-modal").modal("show");
       }


       // hàm xử lý sự kiện confirm delete course trên modal click
       function onBtnConfirmDeleteCourseClick() {
           // B1: thu thập dữ liệu (ko có)
           // B2: validate (ko có)
           // B3: xóa voucher
           deleteCourse(gCoursesId);
           // B4: hiển thị dữ liệu lên front-end (load lại table, ẩn modal)
           alert("Xóa course thành công!");
           onLoadCoursesToTable(gCoursesDB.courses);
           location.reload();
           // ẩn modal confirm xóa đi
           $("#delete-confirm-modal").modal("hide");
       }

       // hàm xóa course theo id course
       function deleteCourse(paramCourseId) {
           var vCourseIndex = getIndexFormCoursesId(paramCourseId);
           gCoursesDB.courses.splice(vCourseIndex, 1);
       }



       // CÁC HÀM DÙNG CHUNG CHO UPDATE VÀ DELETE

       /* get Course index from Course id
              // input: paramCoursesId là CourseId cần tìm index
              / output: trả về chỉ số (index) trong mảng Course */
       function getIndexFormCoursesId(paramCoursesId) {
           var vCourseIndex = -1;
           var vCourseFound = false;
           var vLoopIndex = 0;

           while (!vCourseFound && vLoopIndex < gCoursesDB.courses.length) {
               if (gCoursesDB.courses[vLoopIndex].id === paramCoursesId) {
                   vCourseIndex = vLoopIndex;
                   vCourseFound = true;
               } else {
                   vLoopIndex++;
               }
           }
           return vCourseIndex;
       }

       // hàm dựa vào button detail (edit or delete) xác định đc id courses
       function getCoursesIdFormButton(paramButton) {
           var vTable = $("#courses-table").DataTable()
           var vTableRow = $(paramButton).parents("tr");
           var vCoursesRowData = vTable.row(vTableRow).data();
           return vCoursesRowData.id;
       }